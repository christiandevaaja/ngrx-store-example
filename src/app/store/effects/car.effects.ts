import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { CarActions } from '../actions';
import { switchMap, map, catchError, mergeMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { CarService } from 'src/app/services/car.service';

@Injectable()
export class CarEffects {
    createCar$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CarActions.createCar),
      switchMap(({ carDraft }) =>
        this.carService.createCar(carDraft).pipe(
          map((car) =>
            CarActions.createCarSuccess({
              car,
            })
          )
        )
      )
    )
  );

  constructor(private actions$: Actions, private carService: CarService) {}
}
