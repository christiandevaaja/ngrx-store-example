import { createAction, props } from '@ngrx/store';

export const createCar = createAction(
  '[Car] Create Car',
  props<{ carDraft: any }>()
);

export const createCarSuccess = createAction(
  '[Car] Create Car Success',
  props<{ car: any }>()
);

export const deleteCar = createAction(
  '[Car] Delete Car',
  props<{ carId: string }>()
);
