import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-cars',
  templateUrl: 'cars.component.html',
  styleUrls: ['cars.component.scss'],
})
export class CarsComponent {
  @Input()
  cars!: any[];

  @Output()
  submitDeleteCar: EventEmitter<string> = new EventEmitter();

  deleteCar(carId: string) {
    this.submitDeleteCar.next(carId);
  }
}
